# ECAL-TRK alignment using electrons from Z decay

## Resources
Quick links:

- [Jenkins job](https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Prompt/job/phisym-reco-prod/) 
- [Automation config](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/master/workflows/phisym)

## Workflow structure
Dataset used:

- `/EGamma/*/RAW`

The first step of the calibration runs the reconstruction and saves the sum of the transverse energy (per-crystal, per-run) in a custom NanoAOD output. The reconstruction sequence is defined in CMSSW but requires a custom customize function to be activated (include in the Automation repo).

!!! Note
    The customize function should be included in CMSSW, but efforts to make this work and be compatible with Tier0 operation have failed so far.

The automation workflow structure can be seen in the diagram below.

![wflow-fig](phisym-wflow.png)




