#!/bin/bash

HLTGETCONFIGPATH=`which hltGetConfiguration`

awk '{sub(/# parse command line arguments and options/,"# Special ECAL arguments\n\
parser.add_argument(\047--dumpname\047,\n\
                    dest    = \047dumpname\047,\n\
                    action  = \047store\047,\n\
                    default = \047hlt.py\047,\n\
                    metavar = \047DUMPNAME\047,\n\
                    help    = \047Write dump to this file (the default is %(default)s)\047 )\n\
parser.add_argument(\047--taskstr\047,\n\
                    dest    = \047taskstr\047,\n\
                    action  = \047store\047,\n\
                    default = None,\n\
                    metavar = \047TASKSTR\047,\n\
                    help    = \047The ECAL automation string defining the task.\047 )\n\n\
# parse command line arguments and options")}1' $HLTGETCONFIGPATH \
| awk '{sub(/print\(confdb.HLTProcess\(config\).dump\(\)\)/,"# ECAL customisation\n\
with open(config.dumpname, \047w\047) as fout:\n\
    fout.write(confdb.HLTProcess(config).dump())\n\
    fout.write(\047from customize_hltvalidation import customize_ps_tag \\n\047)\n\
    fout.write(f\047customize_ps_tag(process, taskstr={config.taskstr}, inpt_wflow=\"pulse-shapes-merge-fill\") \\n\047)")}1' > hltGetConfigurationECAL

chmod 755 hltGetConfigurationECAL

