#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
TASK=${3}
EOSDIR=${4}
WDIR=${5}


trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

# export HOME=/afs/cern.ch/user/e/ecalgit/

# source /cvmfs/cms.cern.ch/cmsset_default.sh
# cd $WDIR
# eval $(scram runtime -sh)
# cd -

source /cvmfs/cms.cern.ch/cmsset_default.sh
source /home/ecalgit/setup.sh

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

cp $EOSDIR/config.cfg .
cp $EOSDIR/splitting.json .
#cp $EOSDIR/script.py

python3 script.py $JOBID $EOSDIR

RET=$?
if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${EOSDIR}/results_${JOBID}.pkl"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET

