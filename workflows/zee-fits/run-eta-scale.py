#!/usr/bin/env python3
import sys
import os
import subprocess
import matplotlib.pyplot as plt
import numpy as np
from typing import Optional, List, Tuple
from ijazz import Config
from ijazz.bin.IJazZ import main as ijazz_main
from ijazz.bin.IJazZHDF import display_hdf as ijazz_plots
from ecalautoctrl import JobCtrl, HandlerBase, prev_task_data_source, process_by_intlumi
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

@prev_task_data_source
@process_by_intlumi(target=3000)
class IJazZHandler(HandlerBase):
    """
    Execute all the steps to generate the Zee monitoring plots.
    Process fills that have been dumped (completed).

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

        self.submit_parser.add_argument('--config',
                                        dest='config',
                                        default=None,
                                        type=str,
                                        required=True,
                                        help='Mandatory IJazZ config file')
        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')
 
        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')

    def resubmit(self) -> int:
        """
        Mark failed run for reprocessing.
        """

        # get the new run to process
        runs = self.rctrl.getRuns(status = {self.task : 'processing'})
        # check if any job failed, if so mark all runs for reprocessing
        for run_dict in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run_dict['run_number'],
                                  'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed())>0:
                self.rctrl.updateStatus(run=run_dict['run_number'], status={self.task : 'reprocess'})
                group = jctrl.getJob(jid=0, last=True)[-1]['group'] if 'group' in jctrl.getJob(jid=0, last=True)[-1] else ''
                if group:
                    for r in group.split(','):
                        self.rctrl.updateStatus(run=r, status={self.task : 'reprocess'})

        return 0
        
    def submit(self) -> int:
        """
        Submit new runs.

        :return: status.
        """


        main_cfg = Config(self.opts.config)
        main_cfg.use_absolute_path = True

        for group in self.groups():
            # master run
            run = group[-1]
            # ECALELF produces 4 output files, we only need the main one (the last one in the list)
            ecalelf_files = [lf.split(',')[-1] for lf in self.get_files(group)]
            if ecalelf_files is not None and len(ecalelf_files)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number': run['run_number'], 'fill': run['fill']},
                                dbname=self.opts.dbname)
                if not jctrl.taskExist():
                    jctrl.createTask(jids=[0],
                                     fields=[{
                                         'group': ','.join([r['run_number'] for r in group[:-1]]),
                                         'inputs': ','.join([f.split(',')[-1] for f in ecalelf_files])}])
                try:
                    jctrl.running(jid=0)
                    self.rctrl.updateStatus(run=run['run_number'],
                                            status={self.task: 'processing'})
                    for r in group[:-1]:
                        self.rctrl.updateStatus(run=r['run_number'],
                                                status={self.task: 'merged'})
                                                
                    self.log.info(f'Processing fill: {run["fill"]}')
                    
                    # Run IJazZ routine
                    main_cfg.root_files = ecalelf_files
                    outpath_name = str(self.opts.eosdir)+'/'+str(run['run_number'])+'/'
                    main_cfg.hdf = os.path.abspath(outpath_name+'ijazz_results.hdf5')
                    ijazz_main(main_cfg) 
                    ijazz_plots(main_cfg.hdf,True,None,None, outpath_name)
                    
                    jctrl.done(jid=0, fields={
                            'output': str(main_cfg.hdf)})

                except Exception as e:
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed runnin IJazZ with config {self.opts.config} on run {run["fill"]}: {e}')
                    continue

if __name__ == '__main__':
    handler = IJazZHandler(task='zee-eta-scale',                        
                           prev_input='ecalelf-ntuples-zskim')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(IJazZHandler)
