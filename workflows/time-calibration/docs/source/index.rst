Automation workflow docs: ECAL time calibration
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

runreco.py
==========

.. argparse::
   :filename: ../runreco.py
   :func: get_opts
   :prog: runreco.py

runreco_cc.py
==========

.. argparse::
   :filename: ../runreco_cc.py
   :func: get_opts
   :prog: runreco_cc.py

