#!/bin/bash

FILL=${1}
TAG=${2}
EOSDIR=${3}

PYTHON_PATH=$CMSSW_BASE/src/EcalTiming/EcalTiming/test

# Produce XML and sqlite files
python3 $PYTHON_PATH/makeTimingXML.py --tag=$TAG --calib=$EOSDIR/ecalTiming-corr_$FILL.dat --payload=$(cat $EOSDIR/tmppyload)
python3 $PYTHON_PATH/makeTimingSqlite.py --tag=$TAG --calib=$EOSDIR/ecalTiming-abs_$FILL.xml --firstRun=1

# Remove unnecessary stuff created
rm ./${TAG}_rereco_calib.db
