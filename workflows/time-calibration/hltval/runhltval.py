#!/usr/bin/env python3
import sys
import os
import shutil
import subprocess
from typing import List, Optional
from ecalautoctrl import HTCHandler, process_by_fill, dbs_data_source
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

@dbs_data_source
@process_by_fill(fill_complete=True)
class TimingValidationHandler(HTCHandler):
    """
    Run the HLT rate validation, either with reference prompt conditions or
    with the timing payload under test. With the CC timing option the HLT menu
    is customised to run the CC timing algorithm on CPU only.
    
    :param task: task name. The "ref" or "val" suffix will be automatically
    appended based on the value of the `--ref` command line option. The "-cc" suffix
    will be appended if the --cc-timing command line option is set.
    :param deps_tasks: list of workfow dependencies. The "-cc" suffix will be
    appended if the --cc-timing command line option is set.
    :param dsetname: input RAW dataset names.
    """

    def __init__(self,
                 task: str,
                 deps_tasks: List[str]=None,
                 dsetname: str=None,
                 **kwargs):        
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)

        self.dsetname = dsetname
        
        self.parser.add_argument('--ref',
                                 dest='refcond',
                                 default=False,
                                 action='store_true',
                                 help='Run with reference conditions if set, validation one otherwise')

        self.parser.add_argument('--cc-timing',
                                 dest='cctiming',
                                 default=False,
                                 action='store_true',
                                 help='Run CC timing algorithm if set, ratio timing otherwise')

        
    def __call__(self):
        """
        Execute the command specified by the command line options.
        A suffix is appended to the task name and deps_task names based on the `--ref`
        and --cc-timing options.
        """

        # parse the cmd line to adjust the task name
        self.opts = self.parser.parse_args()
        self.task += 'ref' if self.opts.refcond else 'val'

        # Append task names for CC timing
        if self.opts.cctiming:
            self.task += '-cc'
            self.wdeps = {w + '-cc': self.wdeps[w] for w in self.wdeps.keys()}

        # standard call
        return super().__call__()

if __name__ == '__main__':
    handler = TimingValidationHandler(task='timing-hlt',
                                      deps_tasks=['timing-val'],
                                      dsetname=['/HLTPhysics*/*/RAW', '/HIHLTPhysics*/*/RAW'])

    ret = handler()
    
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(TimingValidationHandler)
