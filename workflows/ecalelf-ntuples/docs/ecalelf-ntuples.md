# ECALELF ntuples production

## Resources
Quick links:

- [Jenkins job](https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Prompt/job/ecalelf-nutples-prod/) 
- [ECALELF code](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/ECALELF)
- [Automation config](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/master/workflows/ecalelf-ntuples)

## Workflow structure
The ECALElf ntuple production is run on two ALCARECO datasets: 

- `/EGamma/*EcalUncalWElectron-Prompt*/ALCARECO`
- `/EGamma/*EcalUncalZElectron-Prompt*/ALCARECO`

These are produce on top of the `/EGamma` primary dataset at T0.
The definition of the ALCARECO sequence can be [here](https://github.com/cms-sw/cmssw/blob/master/Calibration/EcalAlCaRecoProducers/python/ALCARECOEcalUncalIsolElectron_cff.py).

The automation workflow structure can be seen in the diagram below.

![wflow-fig](ecalelf-ntuples-wflow.png)




